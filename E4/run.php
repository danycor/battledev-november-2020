<?php
/*
 * ISOGRAD https://questionsacm.isograd.com/codecontest/pdf/XOREncryption_0270_fr.pdf
 */

global $handle;
$handle = fopen('input4.txt', 'r'); // REPLACE by $handle = STDIN on prod

function uncodeFromBuffers($buffers, $min, $max) {
    $val = 0;
    $i = $min;
    /** @var Buffer $buffer */
    foreach ($buffers as $buffer) {
        while($i+$buffer->step-1 <= $max) {
            $val = $val ^ $buffer->buf[$i];
            $i+=$buffer->step;
        }
    }
    return $val;
}

class Buffer {
    public $step;
    public $buf = [];

    public function __construct($step)
    {
        $this->step = $step;
    }

    public function loadFromBuffer($buffers, $key_size) {
        for($i=0;$i+$this->step-1<$key_size;$i++) {
            $this->buf[] = uncodeFromBuffers($buffers, $i, $i+$this->step-1);
        }
    }
}


function loadKey() {
    global $handle;
    $key_str = fgets($handle);
    $key = explode(" ",$key_str);
    foreach ($key as $ind => $val) {
        $key[$ind] = intval($val);
    }
    return $key;
}

function loadOperators() {
    global $handle;
    $operators = [];
    while(FALSE !== ($line = fgets($handle))) {
        $nbs = explode(" ",$line);
        $operators[] = [
            intval($nbs[0]), intval($nbs[1])
        ];
    }
    return $operators;
}


$first_line = fgets($handle);
$numbers_str = explode(" ", $first_line);

$len_key = intval($numbers_str[0]);
$len_msg = intval($numbers_str[1]);

$key = loadKey();
$operators = loadOperators();


$values = array_fill(0,256,0);

$time = time();

$start_buffer = new Buffer(1);
$start_buffer->buf = $key;

$buffer_sizes = [
    2,8,32,
    128, // 9s
    256, // 6s
    384, // 5s
    512, // 4s
//    896, // 5s
//    1024, // 4s (sans 896)
];

$buffers = [
    1 => $start_buffer
];

foreach ($buffer_sizes as $buffer_size) {
    $b = new Buffer($buffer_size);
    $b->loadFromBuffer($buffers, sizeof($key));
    $buffers[$buffer_size] = $b;

    ksort($buffers);
    $buffers = array_reverse($buffers, true);
}


foreach ($operators as $k => $operator) {
    ++$values[uncodeFromBuffers($buffers, $operator[0], $operator[1])];
}

print time()-$time."sec\n"; // Comment it on prod

print join(" ", $values);