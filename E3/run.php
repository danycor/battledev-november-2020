<?php
$handle = fopen('input4.txt', 'r');
$first_line = fgets($handle);

$lines = [];

class User {
    public $id;
    /**
     * @var User[]
     */
    public $sub_users = [];
    function __construct($id) {
        $this->id = $id;
    }
    function search($id) {
        $user = null;
        if($this->id == $id) {
            $user = $this;
        } else {
            foreach ($this->sub_users as $sub_user) {
                if (($value = $sub_user->search($id)) !== null) {
                    $user = $value;
                    break;
                }
            }
        }
        return $user;
    }
    function getCountInLevel($level) {
        $c = 0;
        if ($level != 0) {
            foreach ($this->sub_users as $sub_user) {
                $c+= $sub_user->getCountInLevel($level-1);
            }
        } else {
            $c = count($this->sub_users);
        }
        return $c;
    }
}

/**
 * @var User[]
 */
$users_pool = [];

while(FALSE !== ($line = fgets($handle))) {
    $line = str_replace("\n", "", $line);
    $ids = explode(" ",$line);
    $ids[0] = intval($ids[0]);
    $ids[1] = intval($ids[1]);
    $sup_user = null;
    $sub_user = null;
    foreach ($users_pool as $key => $user_pool) {
        if($sup_user == null)
            $sup_user = $user_pool->search($ids[1]);
        if($sub_user == null)
            $sub_user = $user_pool->search($ids[0]);
        if($sub_user !== null && $sub_user->id == $user_pool->id) {
            unset($users_pool[$key]);
        }
    }

    if($sub_user == null) {
        $sub_user = new User($ids[0]);
    }
    if($sup_user == null) {
        $sup_user = new User($ids[1]);
        $users_pool[] = $sup_user;
    }
    $sup_user->sub_users[] = $sub_user;
}

$main = null;
foreach ($users_pool as $user) {
    if($user !== null) {
        $main = $user;
    }
}

print "1 ";
print $main->getCountInLevel(0)." ";
print $main->getCountInLevel(1)." ";
print $main->getCountInLevel(2)." ";
print $main->getCountInLevel(3)." ";
print $main->getCountInLevel(4)." ";
print $main->getCountInLevel(5)." ";
print $main->getCountInLevel(6)." ";
print $main->getCountInLevel(7)." ";
print $main->getCountInLevel(8);
