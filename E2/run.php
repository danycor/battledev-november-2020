<?php
$handle = fopen('input4.txt', 'r');
$nb_tweet = intval(fgets($handle));
$nb_night_tweet = 0;
$lines = [];


while(FALSE !== ($line = fgets($handle))) {
    $line = str_replace("\n", "", $line);
    $hour = explode(":", $line);
    $h = intval($hour[0]);
    $m = intval($hour[1]);
    if (
    ($h >= 20 && $m >= 0)
    || ($h <= 7 && $m <= 59)
    ) {
        $nb_night_tweet++;
    }
}
if($nb_night_tweet*2 > $nb_tweet) {
    print "SUSPICIOUS";
} else {
    print "OK";
}
